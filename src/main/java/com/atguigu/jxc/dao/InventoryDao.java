package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.*;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * ClassName: InventoryDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/12 10:30
 * @Version 1.0
 */
public interface InventoryDao {

    void saveDamageList(@Param("damageList") DamageList damageList);

    void saveDamageListGoods(@Param("listGoods") List<DamageListGoods> listGoods);

    void saveOverFlow(@Param("overflowList") OverflowList overflowList);

    void saveOverFlowGoods(@Param("overflowListGoods") OverflowListGoods overflowListGoods);

    List<DamageList> listByTime(@Param("sTime") String sTime,@Param("eTime") String eTime, @Param("user") User user);

    List<DamageListGoods> listByDamageListId(@Param("damageListId") Integer damageListId);

    List<OverflowList> listByOverFlowTime(@Param("sTime") String sTime,@Param("eTime") String eTime);

    List<DamageListGoods> goodsListOverFlow(@Param("overflowListId") Integer overflowListId);
}
