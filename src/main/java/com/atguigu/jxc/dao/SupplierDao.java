package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * ClassName: SupplierDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 14:51
 * @Version 1.0
 */
@Component
public interface SupplierDao {
    List<Supplier> list(@Param("page") Integer page, @Param("rows")Integer rows, @Param("supplierName")String supplierName);

    Supplier query(Integer supplierId);

    Long save(@Param("supplier") Supplier supplier);

    Long update(@Param("supplier") Supplier supplier,@Param("supplierId") Integer supplierId);

    void delete(@Param("idsList") List<String> idsList);
}
