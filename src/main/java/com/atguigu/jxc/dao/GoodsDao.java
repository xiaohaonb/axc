package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
@Component
public interface GoodsDao {


    String getMaxCode();


    List<Goods> listInventory(@Param("page") Integer page, @Param("rows")Integer rows,@Param("codeOrName") String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> list(@Param("page") Integer page,@Param("rows") Integer rows, @Param("goodsName") String goodsName,@Param("goodsTypeId")  Integer goodsTypeId);

    void update(@Param("goodsId") Integer goodsId,@Param("goods") Goods goods);

    void save(@Param("goods") Goods goods);

    Goods listOne(Integer goodsId);

    void delete(Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("page") Integer page,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("page") Integer page,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);


    List<Goods> listAll();

    GoodsType listByTepId(@Param("goodsTypeId") Integer goodsTypeId);


    List<Integer> selectAll(@Param("goodsTypeId") Integer goodsTypeId);
}
