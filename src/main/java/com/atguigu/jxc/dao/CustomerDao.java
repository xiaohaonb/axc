package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * ClassName: CustomerDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 18:07
 * @Version 1.0
 */
public interface CustomerDao {
    List<Customer> query(@Param("page") Integer page, @Param("rows") Integer rows, @Param("customerName") String customerName);

    void save(Customer customer);

    void update(@Param("customerId") Integer customerId,@Param("customer") Customer customer);

    void delete(@Param("asList") List<String> asList);
}
