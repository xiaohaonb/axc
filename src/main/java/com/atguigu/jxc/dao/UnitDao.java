package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * ClassName: UnitDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 19:22
 * @Version 1.0
 */
@Component
public interface UnitDao {
    List<Unit> list();
}
