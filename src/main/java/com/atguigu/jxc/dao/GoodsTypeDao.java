package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void save(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId, @Param("goodsTypeStatus") Integer goodsTypeStatus);

    GoodsType getById(Integer goodsTypeId);


    void delete(@Param("goodsTypeId") Integer goodsTypeId);

    GoodsType select(@Param("goodsTypeId") Integer goodsTypeId);

    List<GoodsType> selectByPid(@Param("pId") Integer pId);

    void update(@Param("goodsType1") GoodsType goodsType1);
}
