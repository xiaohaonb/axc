package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

/**
 * ClassName: UnitService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 19:21
 * @Version 1.0
 */
public interface UnitService {
    List<Unit> list();
}
