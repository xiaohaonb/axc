package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;

import java.util.List;

/**
 * ClassName: InventoryService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/12 10:28
 * @Version 1.0
 */
public interface InventoryService {

    void save(String damageNumber, DamageList damageList, String damageListGoodsStr,User user);

    void saveOverFlowList(String overflowNumber, OverflowList overflowList, String overflowListGoodsStr, User user);

    List<DamageList> listByTime(String sTime, String eTime, User user);

    List<DamageListGoods> listByDamageListId(Integer damageListId);

    List<OverflowList> listByoverflowTime(String sTime, String eTime);

    List<DamageListGoods> goodsListOverFlow(Integer overflowListId);
}
