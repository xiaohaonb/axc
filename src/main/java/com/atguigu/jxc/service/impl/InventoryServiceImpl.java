package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.InventoryDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.InventoryService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.bytebuddy.description.method.MethodDescription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * ClassName: InventoryServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/12 10:29
 * @Version 1.0
 */
@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    InventoryDao inventoryDao;
    @Transactional
    @Override
    public void save(String damageNumber, DamageList damageList, String damageListGoodsStr,User user) {
        //gson转化对象
        Gson gson = new Gson();
        damageList.setUserId(user.getUserId());
        //1 savedamageList
        inventoryDao.saveDamageList(damageList);
        List<DamageListGoods> fruitList = gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>(){}.getType());

        System.out.println(damageList.toString());
        for (DamageListGoods damageListGoods : fruitList) {
            damageListGoods.setDamageListId(damageList.getDamageListId());
        }





        //2.save damagelistGoods
        inventoryDao.saveDamageListGoods(fruitList);


    }
    @Transactional
    @Override
    public void saveOverFlowList(String overflowNumber, OverflowList overflowList, String overflowListGoodsStr, User user) {
        overflowList.setUserId(user.getUserId());
        //修改OverFlow
        inventoryDao.saveOverFlow(overflowList);
        //修改OverFlowGoodsStr
        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoods = gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>(){}.getType());
        for (OverflowListGoods overflowListGood : overflowListGoods) {
            overflowListGood.setOverflowListId(overflowList.getOverflowListId());
            inventoryDao.saveOverFlowGoods(overflowListGood);
        }

    }

    @Override
    public List<DamageList> listByTime(String sTime, String eTime, User user) {

        return inventoryDao.listByTime(sTime,eTime,user);
    }

    @Override
    public List<DamageListGoods> listByDamageListId(Integer damageListId) {


        return inventoryDao.listByDamageListId(damageListId);
    }

    @Override
    public List<OverflowList> listByoverflowTime(String sTime, String eTime) {
        List<OverflowList> list = inventoryDao.listByOverFlowTime(sTime,eTime);


        return list;
    }

    @Override
    public List<DamageListGoods> goodsListOverFlow(Integer overflowListId) {
        return inventoryDao.goodsListOverFlow(overflowListId);
    }
}
