package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName: UnitServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 19:21
 * @Version 1.0
 */
@Service
public class UnitServiceImpl implements UnitService {


    @Autowired
    UnitDao unitDao;
    @Override
    public List<Unit> list() {
        return unitDao.list();
    }
}
