package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {


    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public List<Goods> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        page = page == 0 ? 1 : page;
        page = (page - 1) * rows;
        List<Goods> goods = goodsDao.listInventory(page, rows, codeOrName, goodsTypeId);
        return goods;
    }

    @Override
    public List<Goods> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        page = page == 0 ? 1 : page;
        page = (page - 1) * rows;


        return goodsDao.list(page, rows, goodsName, goodsTypeId);
    }



    @Override
    public void save(Integer goodsId, Goods goods) {
        if (goodsId != null) {
            //更新
            goodsDao.update(goodsId, goods);
        } else {
            //保存
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goods.setLastPurchasingPrice(0.0);
            goodsDao.save(goods);
        }

    }

    /**
     * 删除
     *
     * @param goodsId
     * @return
     */

    @Override
    public ServiceVO deleteById(Integer goodsId) {
        Goods goods = goodsDao.listOne(goodsId);
        Integer state = goods.getState();

        switch (state) {
            case 1:
                return new ServiceVO(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
            case 2:
                return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);

        }

        goodsDao.delete(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, null);
    }

    /**
     * 无库存商品列表
     *
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public List<Goods> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1 : page;
        page = (page - 1) * rows;
        return goodsDao.getNoInventoryQuantity(page, rows, nameOrCode);
    }

    @Override
    public List<Goods> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1 : page;
        page = (page - 1) * rows;
        return goodsDao.getHasInventoryQuantity(page, rows, nameOrCode);
    }

    @Override
    public ServiceVO update(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        Goods goods = goodsDao.listOne(goodsId);
        if (inventoryQuantity != null) {
            goods.setInventoryQuantity(inventoryQuantity);
        }
        if (new Double(purchasingPrice) != null) {
            goods.setPurchasingPrice(purchasingPrice);
        }
        goodsDao.update(goodsId, goods);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {

        Goods goods = goodsDao.listOne(goodsId);
        Integer state = goods.getState();
        switch (state) {
            case 1:
                return new ServiceVO(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
            case 2:
                return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        }

        goodsDao.delete(goodsId);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public List<Goods> listAll() {
        return goodsDao.listAll();
    }


}
