package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName: CustomerServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 18:06
 * @Version 1.0
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerDao customerDao;

    @Override
    public List<Customer> list(Integer page, Integer rows, String customerName) {
        page = page==0?1:page;
        page = (page-1)*rows;
        List<Customer> list = customerDao.query(page,rows,customerName);

        return list;
    }

    /**
     * 更新或保存
     * @param customerId
     * @param customer
     */
    @Override
    public void save(Integer customerId, Customer customer) {
        if (customerId==null){
            customerDao.save(customer);
        }else {
            customerDao.update(customerId,customer);
        }
    }

    @Override
    public void delete(List<String> asList) {

        customerDao.delete(asList);

    }


}
