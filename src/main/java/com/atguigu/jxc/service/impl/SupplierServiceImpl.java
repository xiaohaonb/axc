package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: SupplierServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 14:49
 * @Version 1.0
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    SupplierDao supplierDao;

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public List<Supplier> list(Integer page, Integer rows, String supplierName) {
        page = page==0?1:page;
        page = (page-1)*rows;
        return supplierDao.list(page,rows,supplierName);

    }

    /**
     * 修改或新加供应商
     * @param supplierId
     * @param supplier
     * @return
     */
    @Override
    public void save(Integer supplierId, Supplier supplier) {


        //存在则对其进行修改
        if (supplierId!=null){
            //更新
            supplierDao.update(supplier,supplierId);

        }else {
            //不存在则直接保存
            supplierDao.save(supplier);
        }
    }

    /**
     * 删除
     * @param strings
     */

    @Override
    public void delete(String[] strings) {
        supplierDao.delete(Arrays.asList(strings));
    }


}
