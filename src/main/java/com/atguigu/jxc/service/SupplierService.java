package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;

/**
 * ClassName: SupplierService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 14:48
 * @Version 1.0
 */
public interface SupplierService {
    List<Supplier> list(Integer page, Integer rows, String supplierName);

    void save(Integer supplierId, Supplier supplier);

    void delete(String[] strings);
}
