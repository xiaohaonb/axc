package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import io.swagger.models.auth.In;

import java.util.List;
import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();




    List<Goods> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    List<Goods> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void save(Integer goodsId, Goods goods);

    ServiceVO deleteById(Integer goodsId);

    List<Goods> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    List<Goods> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    ServiceVO update(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);


    ServiceVO deleteStock(Integer goodsId);

    List<Goods> listAll();


}
