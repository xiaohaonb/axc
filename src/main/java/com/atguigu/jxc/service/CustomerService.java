package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.List;

/**
 * ClassName: CustomerService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 18:06
 * @Version 1.0
 */
public interface CustomerService {
    List<Customer> list(Integer page, Integer rows, String customerName);


    void save(Integer customerId, Customer customer);

    void delete(List<String> asList);
}
