package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: CustomerContorller
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 18:00
 * @Version 1.0
 */
@RestController
@RequestMapping("/customer")
public class CustomerContorller {

    @Autowired
    CustomerService customerService;


    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam("ids") String ids){
        String[] split = ids.split(",");

        customerService.delete(Arrays.asList(split));


        return new ServiceVO(100,"请求成功",null);
    }
    /**
     * 查询
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(@RequestParam("page")Integer page,
                                   @RequestParam("rows") Integer rows,
                                   @RequestParam(value = "customerName",required = false)  String  customerName){
        List<Customer> list =  customerService.list(page,rows,customerName);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",list.size());
        map.put("rows",list);

        return map;
    }


    @PostMapping("/save")
    public ServiceVO save(
            @RequestParam(value = "customerId",required = false) Integer customerId,
            Customer customer){
        customerService.save(customerId,customer);
        return new ServiceVO(100,"请求成功",null);
    }
}
