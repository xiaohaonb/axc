package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: SupplierController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 14:40
 * @Version 1.0
 */
@RequestMapping("/supplier")
@RestController
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    /**
     * 批量删除 TDOO
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam("ids") String ids){
        //todo

        String[] strings = ids.split(",");
        supplierService.delete(strings);
        ServiceVO<Object> serviceVO = new ServiceVO<>(100, "请求成功", null);

        return serviceVO;

    }

    /**
     * 分页查询供应商
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> list(@RequestParam("page") Integer page,
                                    @RequestParam("rows") Integer rows,
                                    @RequestParam(value = "supplierName", required = false) String supplierName) {
        List<Supplier> supplierList = supplierService.list(page, rows, supplierName);
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("total", supplierList.size());
        hashMap.put("rows", supplierList);


        return hashMap;
    }

    /**
     * 更新或修改
     * @param supplierId
     * @param supplier
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(@RequestParam(value = "supplierId",required = false) Integer supplierId, Supplier supplier) {

        supplierService.save(supplierId, supplier);
        ServiceVO<String> serviceVO = new ServiceVO<>(100, "请求成功", null);
        return serviceVO;

    }
}
