package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: InventoryController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/12 10:13
 * @Version 1.0
 */
@RestController
public class InventoryController {

    @Autowired
    InventoryService inventoryService;


    @PostMapping("/damageListGoods/save")
    public ServiceVO save(String damageNumber, DamageList damageList, String damageListGoodsStr,HttpSession httpSession){
        User user = (User)httpSession.getAttribute("currentUser");

        inventoryService.save(damageNumber,damageList,damageListGoodsStr,user);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }


    @PostMapping("/overflowListGoods/save")
    public ServiceVO save1(String overflowNumber,OverflowList overflowList, String overflowListGoodsStr,HttpSession httpSession){

        User user = (User)httpSession.getAttribute("currentUser");
        inventoryService.saveOverFlowList(overflowNumber,overflowList,overflowListGoodsStr,user);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

    }
    /**
     * 报损单查询
     *
     */
    @PostMapping("/damageListGoods/list")
    public Map<String,Object> list(String sTime,String eTime,HttpSession session){
        User user = (User) session.getAttribute("currentUser");
        List<DamageList> list =  inventoryService.listByTime(sTime,eTime,user);
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",list);

        return map;
    }
    /**
     * 查询报损商品的商品信息
     */

    @PostMapping("/damageListGoods/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        List<DamageListGoods> list = inventoryService.listByDamageListId(damageListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
    /**
     * 查询报溢出单
     */
    @PostMapping("/overflowListGoods/list")
    public Map<String,Object> overflowListGoodsList(String  sTime,String  eTime){
        List<OverflowList> list = inventoryService.listByoverflowTime(sTime,eTime);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
    /**
     * 报溢商品信息
     */
    @PostMapping("/overflowListGoods/goodsList")
    public Map<String,Object> goodsListoverflowListGoods(Integer overflowListId){
        List<DamageListGoods> damageListGoodsList = inventoryService.goodsListOverFlow(overflowListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",damageListGoodsList);
        return map;
    }
}
