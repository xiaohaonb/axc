package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;

import com.sun.org.apache.bcel.internal.generic.NEW;
import io.swagger.models.auth.In;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.crypto.Data;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RequestMapping("/goods")
@RestController
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/listInventory")
    public Map<String,Object> listInventory(@RequestParam("page")Integer page,
                                            @RequestParam("rows")Integer rows,
                                            @RequestParam(value = "codeOrName",required = false) String codeOrName,
                                            @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId){


        List<Goods> goods = goodsService.listInventory(page, rows, codeOrName, goodsTypeId);
        Map<String, Object> listHashMap = new HashMap<>();
        listHashMap.put("total",goods);
        listHashMap.put("rows",goods);


        return listHashMap;

    }


    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(@RequestParam("page") Integer page,
                                   @RequestParam("rows") Integer rows,
                                   @RequestParam(value = "goodsName",required = false) String goodsName,
                                   @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId){

        List<Goods> goodsList = goodsService.list(page,rows,goodsName,goodsTypeId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",goodsList.size());
        map.put("rows",goodsList);


        return map;

    }

    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goodsId 商品信息实体
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(@RequestParam(value = "goodsId",required = false) Integer goodsId,Goods goods){


        goodsService.save(goodsId,goods);

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);

    }

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(Integer goodsId){


        ServiceVO serviceVO = goodsService.deleteById(goodsId);

        return serviceVO;

    }
    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(@RequestParam("page") Integer page,@RequestParam("rows") Integer rows,@RequestParam(value = "nameOrCode",required = false) String nameOrCode){
        List<Goods> goodsList = goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
        Map<String, Object> map = new HashMap<>();
        map.put("total",goodsList.size());
        map.put("rows",goodsList);
        return map;
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(
            Integer page,
            Integer rows,
            @RequestParam(value = "nameOrCode",required = false) String nameOrCode ){

        Map<String, Object> map = new HashMap<>();

        List<Goods> goodsList = goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
        map.put("total",goodsList.size());
        map.put("rows",goodsList);

        return map;
    }


    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */

    @PostMapping("/saveStock")
    public ServiceVO saveStock(@RequestParam("goodsId") Integer goodsId,
                               @RequestParam("inventoryQuantity") Integer inventoryQuantity,
                               @RequestParam("purchasingPrice") double purchasingPrice){


        return goodsService.update(goodsId,inventoryQuantity,purchasingPrice);
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(Integer goodsId){
        return goodsService.deleteStock(goodsId);
    }

    /**
     * 查询库存报警商品信息
     * @return
     */

    @PostMapping("/listAlarm")
    public Map<String,Object> listAlarm(){
        List<Goods> goodsList = goodsService.listAll();
        Map<String, Object> map = new HashMap<>();
        map.put("rows",goodsList);

        return map;
    }


}
