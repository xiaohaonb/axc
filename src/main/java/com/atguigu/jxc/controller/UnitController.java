package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: UnitController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author 郑浩
 * @Create 2023/9/11 19:16
 * @Version 1.0
 */
@RestController
@RequestMapping("/unit")
public class UnitController {


    @Autowired
    UnitService unitService;

    @PostMapping("/list")
    public Map<String,Object> list(){
        List<Unit> unitList = unitService.list();
        Map<String, Object> map = new HashMap<>();
        map.put("rows",unitList);

        return map;


    }


}
